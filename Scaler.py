import json
import os
import numpy as np

class Scaler:

    def __init__(self, values):
        self.min_val = values[0]
        self.max_val = values[1]

    def get_scaler(data_train):
        max_val = max(data_train[0]["in"])
        min_val = min(data_train[0]["in"])

        for d in data_train:
            max_val = max(max_val, np.max(d["in"]))
            min_val = min(min_val, np.min(d["in"]))

        return Scaler((min_val,max_val))

    def scale_data(self,data):

        for i in range(len(data)):
            data[i]["in"] = self.scale(data[i]["in"])
            data[i]["out"] = self.scale(data[i]["out"])

    def scale(self,arr):
        return 2*(arr-self.min_val)/(self.max_val-self.min_val) - 1

    def rev_scale(self,arr):
        return (arr+1)*(self.max_val-self.min_val)/2+self.min_val

    def save(self,path):
        js = json.dumps(self.__dict__)
        f = open(os.path.join(path,"scaler.json"), 'w')
        f.write(js)
        f.close()

    def load(path):
        f = open(os.path.join(path,"scaler.json"))
        d = json.load(f)
        f.close()

        return Scaler((d["min_val"], d["max_val"]))

    def __str__(self):

        return "("+str(self.min_val)+", "+str(self.max_val)+")"