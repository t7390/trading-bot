# Trading bot

Trading bot with a reinforcement learning algorithm.

## Getting started

### Create the environment

 Open terminal in the root directory of the project.
 Run:

```
python3 -m venv env/
```

### Activate the environment

On linux:
```
source env/bin/activate
```
On windows:
```
env\\Scripts\\activate.bat
```

### Install dependencies

```
pip install -r requirements.txt
```



