import tensorflow as tf
import matplotlib.pyplot as plt
import os

save_path = "Models/Regressions"

def CreateLSTM(batch_size, span):
    input_size = span
    hide_size = 300
    lstm = tf.keras.Sequential()
    lstm.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(hide_size, return_sequences=False), input_shape=(input_size,1)))
    #lstm.add(tf.keras.layers.LSTM(hide_size))
    lstm.add(tf.keras.layers.Dropout(0.5))
    lstm.add(tf.keras.layers.Dense(hide_size//2, activation='tanh'))
    lstm.add(tf.keras.layers.Dropout(0.5))
    lstm.add(tf.keras.layers.Dense(hide_size//4, activation='relu'))
    lstm.add(tf.keras.layers.Dropout(0.5))
    lstm.add(tf.keras.layers.Dense(1, activation='linear'))

    return lstm

def train(model, ds_est, ds_val):
    training = model.fit(ds_est, epochs=20, validation_data=ds_val)

    return training

def plot_res(training):

    print(training.history.keys())
    estim_loss = training.history["loss"]
    valid_loss = training.history["val_loss"]

    plt.figure("Train results")
    plt.plot(estim_loss, label="estim")
    plt.plot(valid_loss, label="valid")
    plt.title("Etimation and Validation loss")
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.legend()


def save_model(model, name, scaler):
    model_path = os.path.join(save_path,name)
    model.save(model_path)
    scaler.save(model_path)


if __name__ == "__main__":
    CreateLSTM(32, 30)