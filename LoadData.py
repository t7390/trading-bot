import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import random

np.random.seed(1)

#Index definissant les parties des données utilisé pour la partie supervsée et la partie RL
#(depart, fin)
supervised_index = (0,50000)
rl_index = (supervised_index[1], supervised_index[1]+100000)

def LoadData(file_name, span, supervised=True, plot=True):

    #Read csv
    data = pd.read_csv(file_name)

    #Get specific columns
    data = data[["Date", "Close"]]

    #Transform to datetime
    data["Date"] = pd.to_datetime(data["Date"])

    #Plot data
    if plot:
        plt.figure("Data")
        plt.plot(data["Date"], data["Close"])

    #Cut data supervised/RL
    if supervised:
        cut_index = supervised_index
    else:
        cut_index = rl_index

    cut_index = (cut_index[0],min(cut_index[1], len(data)))

    # Reformat data
    data_in_out = []
    print("number of observations:", len(data))
    print("Cut index shape:", cut_index)
    for i in range(span+cut_index[0], cut_index[1]):
        d = {"date": data["Date"][i], "id": i, "in": [], "out": 0}

        d["in"] = np.array(list(data["Close"][i-span:i]))
        d["in"] = np.reshape(np.array(d["in"]), (span,1))
        d["out"] = data["Close"][i]
        d["out"] = np.array(d["out"])
        
        data_in_out.append(d)

    return data_in_out

def get_train_test(data_in_out, shuffle=True, split_ratio=0.8):
    split_ratio2 = split_ratio-(1-split_ratio)
    n_split1 = int(len(data_in_out)*split_ratio2)
    n_split2 = int(len(data_in_out)*split_ratio)

    data_plot = data_in_out[n_split2:]

    data_in_out = data_in_out[:n_split2]

    # Shuffle inputs
    if shuffle:
        random.shuffle(data_in_out)

    data_train = data_in_out[:n_split1]

    data_test = data_in_out[n_split1:]


    return data_train, data_test, data_plot


def create_dataset(data):
  nsamples = len(data) # number of documents
  span = len(data[0]["in"])

  in_tf = np.empty([nsamples, span, 1], dtype="float32")
  out_tf = np.zeros([nsamples, 1], dtype="float32")

  # [TODO] fill in data et label with the appropriate data
  for i, d in enumerate(data):
    in_tf[i] = d["in"]
    out_tf[i] = d["out"]

  ds = tf.data.Dataset.from_tensor_slices((in_tf, out_tf))

  # visualize the dataset and its elements
  #print(ds)
  #print(ds.element_spec)
  #print(ds.cardinality().numpy())

#   for item in ds.take(1):
#     print(item)

  return ds

def split_est_val(ds_train, split_ratio=0.8, batch_size=32):
    # Number of observation
    n = ds_train.cardinality().numpy()

    # shuffle data, possibly each iteration through ds
    ds_train = ds_train.shuffle(n, reshuffle_each_iteration=False)

    # Split data in Validation and Estimation
    n_80 = int(n*split_ratio)

    ds_est = ds_train.take(n_80)
    ds_val = ds_train.skip(n_80)

    # batch data into batches of 128 samples, with access optimization
    ds_est = ds_est.batch(batch_size,drop_remainder=True).prefetch(batch_size).cache()
    ds_val = ds_val.batch(batch_size,drop_remainder=True).prefetch(batch_size).cache()

    #print(ds_est)
    print(ds_est.cardinality().numpy())
    #print(ds_val)
    print(ds_val.cardinality().numpy())

    return ds_est, ds_val