import random
import numpy as np

class DiscretSpace:

    def __init__(self, num_state):
        self.n = num_state

    def sample(self):
        state = random.randint(0, self.n-1)
        return state

    def __str__(self):
        return "discrete ("+str(self.n)+")"

class ContinuousSpace:

    def __init__(self, shape):
        self.shape = (shape,)

    def __str__(self):
        return "continuous ("+str(self.shape)+")"

    