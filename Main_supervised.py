
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
from ast import Load
from venv import create
from LoadData import *
from NeuralNetwork import *
from SelectedLSTM import *

import tensorflow as tf

batch_size = 128
span = 30

# Data
#data_in_out = LoadData("Data/ETH-USD_full.csv",plot=False)
data_in_out = LoadData("Data/Binance_ETHUSDT_minute.csv",span,supervised=True,plot=False)

data_train, data_test, data_plot = get_train_test(data_in_out)

print("Number of observation in training:",len(data_train))

# scale
scaler = Scaler.get_scaler(data_train)
print("min and max values in traning:",scaler)

scaler.scale_data(data_train)
scaler.scale_data(data_test)

ds_train = create_dataset(data_train)
ds_test = create_dataset(data_test)

ds_test = ds_test.batch(batch_size,drop_remainder=True).prefetch(batch_size).cache()

ds_est, ds_val = split_est_val(ds_train, batch_size=batch_size)

#Model
def train_lstm():
    lstm = CreateLSTM(batch_size, span=span)

    #compile
    lstm.compile(optimizer='adam', loss='mse')
    print(lstm.summary())

    training = train(lstm, ds_est, ds_val)

    plot_res(training)

    return lstm

lstm = train_lstm()
save_model(lstm,"lstm2",scaler)
lstm,scaler = load_model()


#test
test = lstm.evaluate(ds_test)
print("test lost rescale:", scaler.rev_scale(np.sqrt(test))-scaler.rev_scale(0))

#plot pred
#cut data test multiple of batchsize

data_plot = data_plot[:1000]

data_in = np.array([d["in"] for d in data_plot])
data_in = scaler.scale(data_in)
data_out = np.array([d["out"] for d in data_plot])

pred = lstm.predict(data_in, batch_size=1).flatten()

pred = scaler.rev_scale(pred)


print(pred.shape)
print(data_out.shape)
plt.figure("Test result")
plt.plot(pred, label="pred")
plt.plot(data_out, label="true")
plt.plot(scaler.rev_scale(data_in[:,-1]), label="in")
plt.legend()
plt.grid()
plt.show()

num_next = span

# plt.figure("Pred next")
# plt.plot(data_out, label="true")
# for i in range(10):#len(data_out)//num_next):
#     pred_next = predict_next(lstm, np.copy(data_in[i*num_next]), num_next)
#     pred_next = np.array(pred_next)
#     pred_next = scaler.rev_scale(pred_next)
#     plt.plot(list(range(i*num_next,i*num_next+num_next)),pred_next, label="pred")
# plt.legend()
# plt.show()


