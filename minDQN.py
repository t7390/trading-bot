"""
A Minimal Deep Q-Learning Implementation (minDQN)
Running this code will render the agent solving the CartPole environment using OpenAI gym. Our Minimal Deep Q-Network is approximately 150 lines of code. In addition, this implementation uses Tensorflow and Keras and should generally run in less than 15 minutes.
Usage: python3 minDQN.py
"""
import tensorflow as tf
import numpy as np
from tensorflow import keras

from collections import deque
import time
import random
import matplotlib.pyplot as plt

from Environment import *

RANDOM_SEED = 5
tf.random.set_seed(RANDOM_SEED)
np.random.seed(RANDOM_SEED)

### LOAD DATA ####
span = Env.span
data_in_out = LoadData("Data/Binance_ETHUSDT_minute.csv",span,supervised=False,plot=False)
data_train, data_test, data_plot = get_train_test(data_in_out, shuffle=False)

### ENV TRAIN ####
env = Env(data_train)

print("Action Space: {}".format(env.action_space))
print("State space: {}".format(env.observation_space))

# An episode a full game
train_episodes = 100
test_episodes = 100
number_of_render = 100

save_path = "Models/RL"

def agent(state_shape, action_shape):
    """ The agent maps X-states to Y-actions
    e.g. The neural network output is [.1, .7, .1, .3]
    The highest value 0.7 is the Q-Value.
    The index of the highest action (0.7) is action #1.
    """
    #learning_rate = 0.001
    init = tf.keras.initializers.HeUniform()
    model = keras.Sequential()
    model.add(keras.layers.Dense(24, input_shape=state_shape, activation='relu', kernel_initializer=init))
    model.add(keras.layers.Dense(12, activation='relu', kernel_initializer=init))
    model.add(keras.layers.Dense(6, activation='relu', kernel_initializer=init))
    model.add(keras.layers.Dense(action_shape, activation='linear', kernel_initializer=init))
    optimizer = tf.keras.optimizers.Adam()#learning_rate=learning_rate)
    model.compile(loss=tf.keras.losses.Huber(), optimizer=optimizer, metrics=['accuracy'])
    return model

def get_qs(model, state, step):
    return model.predict(state.reshape([1, state.shape[0]]))[0]

def train(env, replay_memory, model, target_model, done):
    learning_rate = 0.5 # Learning rate
    discount_factor = 1

    MIN_REPLAY_SIZE = 1000
    if len(replay_memory) < MIN_REPLAY_SIZE:
        return

    batch_size = 64 * 2
    mini_batch = random.sample(replay_memory, batch_size)
    current_states = np.array([transition[0] for transition in mini_batch])
    current_qs_list = model.predict(current_states)
    new_current_states = np.array([transition[3] for transition in mini_batch])
    future_qs_list = target_model.predict(new_current_states)

    X = []
    Y = []
    for index, (observation, action, reward, new_observation, done) in enumerate(mini_batch):
        if not done:
            max_future_q = reward + discount_factor * np.max(future_qs_list[index])
        else:
            max_future_q = reward

        current_qs = current_qs_list[index]
        current_qs[action] = (1 - learning_rate) * current_qs[action] + learning_rate * max_future_q

        X.append(observation)
        Y.append(current_qs)
    training = model.fit(np.array(X), np.array(Y), epochs=1, batch_size=batch_size, verbose=0, shuffle=True)
    
    #print(training.history["accuracy"])
    
def train_DQN():
    epsilon = 1 # Epsilon-greedy algorithm in initialized at 1 meaning every step is random at the start
    max_epsilon = 1 # You can't explore more than 100% of the time
    min_epsilon = 0.001 # At a minimum, we'll always explore 0.1% of the time

    reward_list = []

    # 1. Initialize the Target and Main models
    # Main Model (updated every 4 steps)
    model = agent(env.observation_space.shape, env.action_space.n)
    # Target Model (updated every 100 steps)
    target_model = agent(env.observation_space.shape, env.action_space.n)
    target_model.set_weights(model.get_weights())

    replay_memory = deque(maxlen=100_000)

    update_counter = 0

    steps_to_update_target_model = 0

    for episode in range(train_episodes):
        total_training_rewards = 0
        observation = env.reset()
        done = False
        update_counter = 0
        while not done:
            update_counter += 1
            steps_to_update_target_model += 1

            random_number = np.random.rand()
            # 2. Explore using the Epsilon Greedy Exploration Strategy
            if random_number <= epsilon:
                # Explore
                action = env.action_space.sample()
            else:
                # Exploit best known action
                # model dims are (batch, env.observation_space.n)
                encoded = observation
                encoded_reshaped = encoded.reshape([1, encoded.shape[0]])
                predicted = model.predict(encoded_reshaped).flatten()
                action = np.argmax(predicted)
            new_observation, reward, done, info = env.step(action)
            replay_memory.append([observation, action, reward, new_observation, done])

            # 3. Update the Main Network using the Bellman Equation
            if steps_to_update_target_model % 4 == 0 or done:
                train(env, replay_memory, model, target_model, done)

            observation = new_observation
            total_training_rewards += reward

            if done:
                reward_list.append(total_training_rewards/update_counter)
                print('Total training rewards: {} after n steps = {} with final reward = {}'.format(total_training_rewards, episode, reward))

                if steps_to_update_target_model >= 100:
                    print('Copying main network weights to the target network weights. Epsilon value:', round(epsilon,2))
                    target_model.set_weights(model.get_weights())
                    steps_to_update_target_model = 0
                break
        if train_episodes//number_of_render != 0 and episode % (train_episodes//number_of_render) == 0:
            env.render("train")
        epsilon = np.exp(np.log(min_epsilon/max_epsilon)/train_episodes*episode+np.log(max_epsilon))
    
    name = "DQN_RELU_5"
    model_path = os.path.join(save_path,name)
    model.save(model_path)

    fig = plt.figure("Mean reward")
    plt.plot(reward_list)
    plt.title("Mean reward")
    plt.xlabel("Episode")
    plt.ylabel("Mean reward")
    plt.savefig("reward_train_DQN.pdf", format="pdf")
    plt.close(fig)

def load_model():
    name = "DQN_RELU_5"
    model_path = os.path.join(save_path,name)
    model = tf.keras.models.load_model(model_path)
    return model

def test():
    model = load_model()

    ### ENV TEST ####
    env = Env(data_test)

    reward_list = []

    for episode in range(test_episodes):
        total_rewards = 0
        observation = env.reset()
        done = False
        update_counter = 0
        while not done:
            update_counter += 1
            # Exploit best known action
            # model dims are (batch, env.observation_space.n)
            encoded = np.copy(observation)
            encoded_reshaped = encoded.reshape([1, encoded.shape[0]])
            predicted = model.predict(encoded_reshaped).flatten()
            action = np.argmax(predicted)
            new_observation, reward, done, info = env.step(action)

            observation = new_observation
            total_rewards += reward

            if done:
                reward_list.append(total_rewards/update_counter)
                print('Total testing rewards: {} after n steps = {} with final reward = {}'.format(total_rewards, episode, reward))
                env.render("test")
                break
    
    fig = plt.figure("Distribution of mean reward")
    plt.hist(reward_list, bins=20)
    plt.title("Distribution of mean reward")
    plt.xlabel("Episode")
    plt.ylabel("Mean reward")
    plt.axvline(sum(reward_list)/len(reward_list), color='k', linestyle='dashed', linewidth=1)
    plt.savefig("reward_test_DQN.pdf", format="pdf")
    plt.close(fig)



if __name__ == '__main__':
    #train_DQN()
    test()
