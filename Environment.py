from LoadData import *
from SelectedLSTM import *
from Space import *

import random

class Env:

    #supervised
    span = 30

    #cash
    eth_bounds = (1,20)
    usd_bounds = (1,20)

    buy_amount = 0.1
    sale_amount = 0.1

    #fees
    fee = 0.001

    #Space
    action_space = DiscretSpace(3)
    observation_space = ContinuousSpace(36)

    #episode
    episode_sizes = (10*60,10*120)

    #render
    fig_dir_pref = "fig_"

    def __init__(self,data):

        self.data_length = len(data)

        print("Number of observation in training:",self.data_length)

        self.data = data

        #Load supervised model
        self.lstm,self.scaler = load_model() 

        self.render_i = 0

        self.reset()

    def get_obs(self):
        #scaled_s = self.scaler.scale(self.s[self.i_step])
        #return np.array([scaled_s,self.preds[self.i_step][0], self.eth, self.usd])

        #30 last shares
        share = self.data_in[self.i_step].flatten()
        #predictions
        actual_pred = self.preds[self.i_step-1]
        pred = self.preds[self.i_step]
        #get last trade
        last_action = self.last_action
        return np.concatenate((share,[actual_pred, pred, *last_action, self.eth, self.usd]))

    def reset(self):

        #init cash
        self.eth = random.randint(self.eth_bounds[0], self.eth_bounds[1])
        self.usd = random.randint(self.usd_bounds[0], self.usd_bounds[1])
        self.eth_list = [self.eth]
        self.usd_list = [self.usd]

        #select part of data for the episode
        self.ep_length = random.randint(self.episode_sizes[0], self.episode_sizes[1])
        index_cut = random.randint(1, self.data_length-self.ep_length)

        data_in = self.data[index_cut-1:index_cut+self.ep_length]
        self.data_in = np.array([d["in"] for d in data_in])
        self.s = self.data_in[:,-1].flatten()

        #scale data_in
        self.data_in = self.scaler.scale(self.data_in)

        self.preds = self.lstm.predict(self.data_in, batch_size=1).flatten()

        #actions
        self.actions = [] #c_i
        self.last_action = [0.0,0.0] # last c, last corresponding s

        #rewards
        self.rewards = [] #r_i
        self.moneys = [] 
        
        self.i_step = 1

        return self.get_obs()

    def step(self, action):
        
        actual_s = self.s[self.i_step]
        
        # #fixed buy sale
        # c = 0.0
        # if action == 0: #sale
        #     c = min(self.sale_amount,self.eth)
        # elif action == 1: #buy
        #     c = -min(self.buy_amount,self.usd)/actual_s
        # else: # hold
        #     c = 0.0

        #full buy sale
        c = 0.0
        if action == 0: #sale
            c = self.eth/(self.fee*(c>0)+1)
        elif action == 1: #buy
            c = -self.usd/actual_s/(self.fee*(c<0)+1)
        else: # hold
            c = 0.0

        self.actions.append(c)

        #last action
        if c != 0.0:
            self.last_action[0] = c
            self.last_action[1] = self.scaler.scale(self.s[self.i_step])

        #update cash
        self.eth -= (self.fee*(c>0)+1)*c
        self.usd += (self.fee*(c<0)+1)*actual_s*c
        self.eth_list.append(self.eth)
        self.usd_list.append(self.usd)

        self.i_step += 1 
        reward = self.reward()
        self.rewards.append(reward)
        done = False
        if self.i_step == self.ep_length-1:
            done = True
        new_obs = self.get_obs()
        self.moneys.append(self.money())

        return new_obs, reward, done, None


    def reward(self):
        c = np.array(self.actions)
        r = np.sum(((self.fee*(c<0)+1)*self.s[1:self.i_step]-(self.fee*(c>0)+1)*self.s[self.i_step])*c)
        r_prev = 0
        if self.i_step > 1:
            c = c[:-1]
            r_prev = np.sum(((self.fee*(c<0)+1)*self.s[1:self.i_step-1]-(self.fee*(c>0)+1)*self.s[self.i_step-1])*c)
        return r-r_prev

    def money(self):
        c = np.array(self.actions)
        r = np.sum(((self.fee*(c<0)+1)*self.s[1:self.i_step]-(self.fee*(c>0)+1)*self.s[self.i_step])*c)
        return r

    def render(self,name):
        fig_dir = self.fig_dir_pref+name+"/"
        if self.render_i == 0: #clear dir
            if not os.path.exists(fig_dir):
                os.mkdir(fig_dir)
            for f in os.listdir(fig_dir):
                os.remove(os.path.join(fig_dir, f))

        fig = plt.figure("Env", figsize=(16,10))
        gs = fig.add_gridspec(5, hspace=0)
        axs = gs.subplots(sharex=True)
        fig.suptitle('Env'+str(self.render_i))
        axs[0].plot(self.s[1:], label="real")
        axs[0].plot(self.scaler.rev_scale(self.preds[1:]))
        axs[0].legend()
        index_of_sale = np.nonzero(np.array(self.actions) > 0)[0]
        index_of_buy = np.nonzero(np.array(self.actions) < 0)[0]
        axs[0].plot(index_of_buy,self.s[index_of_buy+1],'go')
        axs[0].plot(index_of_sale,self.s[index_of_sale+1],'ro')
        axs[1].plot(self.eth_list)
        axs[2].plot(self.usd_list)
        axs[3].plot(self.rewards)
        axs[4].plot(self.moneys)

        # Hide x labels and tick labels for all but bottom plot.
        for ax in axs:
            ax.label_outer()
        fig_path = os.path.join(fig_dir,"ep_"+str(self.render_i)+".pdf")
        plt.savefig(fig_path, format="pdf")
        plt.close(fig)
        
        self.render_i += 1



