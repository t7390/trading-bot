import os
import numpy as np
import tensorflow as tf

from Scaler import *


name = "lstm1"

path = os.path.join("Models/Regressions", name)

def load_model():
    lstm = tf.keras.models.load_model(path)
    scaler = Scaler.load(path)
    return lstm,scaler

# predict multiple values forward
def predict_next(lstm, data_in, num):
    data_out = []
    data = np.array([data_in])
    for i in range(num):
        out = lstm.predict(data, batch_size=1)
        data_out.append(out[0])
        data = np.array([np.concatenate((data[0][1:],out), axis=0)])
    return data_out